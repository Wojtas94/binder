/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.model.dto;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author wojciech.misiaszek
 */
public class TaskByDateDto {

    private String nameOfProject;
    private String numberOfProject;
    private String nameOfTask;
    private String numberOfTask;
    private Date timeSpentOnproject;
    private LocalDate taskDate;

    public TaskByDateDto(String nameOfProject, String numberOfProject, String nameOfTask, String numberOfTask, Date timeSpentOnproject, LocalDate taskDate) {
        this.nameOfProject = nameOfProject;
        this.numberOfProject = numberOfProject;
        this.nameOfTask = nameOfTask;
        this.numberOfTask = numberOfTask;
        this.timeSpentOnproject = timeSpentOnproject;
        this.taskDate = taskDate;
    }

    public String getNameOfProject() {
        return nameOfProject;
    }

    public void setNameOfProject(String nameOfProject) {
        this.nameOfProject = nameOfProject;
    }

    public String getNumberOfProject() {
        return numberOfProject;
    }

    public void setNumberOfProject(String numberOfProject) {
        this.numberOfProject = numberOfProject;
    }

    public String getNameOfTask() {
        return nameOfTask;
    }

    public void setNameOfTask(String nameOfTask) {
        this.nameOfTask = nameOfTask;
    }

    public String getNumberOfTask() {
        return numberOfTask;
    }

    public void setNumberOfTask(String numberOfTask) {
        this.numberOfTask = numberOfTask;
    }

    public Date getTimeSpentOnproject() {
        return timeSpentOnproject;
    }

    public void setTimeSpentOnproject(Date timeSpentOnproject) {
        this.timeSpentOnproject = timeSpentOnproject;
    }

    public LocalDate getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(LocalDate taskDate) {
        this.taskDate = taskDate;
    }

    @Override
    public String toString() {
        return "TaskByDateDto{" + "nameOfProject=" + nameOfProject + ", numberOfProject=" + numberOfProject + ", nameOfTask=" + nameOfTask + ", numberOfTask=" + numberOfTask + ", timeSpentOnproject=" + timeSpentOnproject + ", taskDate=" + taskDate + '}';
    }
    
    
    
    
    
    
}
