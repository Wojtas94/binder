/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.mggp.binder.model.entity;

import java.io.Serializable;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author wojciech.misiaszek
 */
@Entity
@Table(name = "job_task_card")
public class JobCardTaskEntity implements Serializable{
    
    private Long id;
    private Time workTime;
    private UserEntity userEntity;
    private ProjectEntity projectEntity;
    private TaskEntity taskEntity;
    private LocalDate dateOfWorkOnTask;

    public JobCardTaskEntity() {
    }

    public JobCardTaskEntity(Long id, Time workTime, UserEntity userEntity, ProjectEntity projectEntity, LocalDate dateOfWorkOnTask) {
        this.id = id;
        this.workTime = workTime;
        this.userEntity = userEntity;
        this.projectEntity = projectEntity;
        this.dateOfWorkOnTask = dateOfWorkOnTask;
    }

    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "work_time")
    public Time getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Time workTime) {
        this.workTime = workTime;
    }

    @ManyToOne
    @JoinColumn(name = "created_by")
    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    @ManyToOne
    @JoinColumn(name = "project_id")
    public ProjectEntity getProjectEntity() {
        return projectEntity;
    }

    public void setProjectEntity(ProjectEntity projectEntity) {
        this.projectEntity = projectEntity;
    }

    @Column(name = "created")
    public LocalDate getDateOfWorkOnTask() {
        return dateOfWorkOnTask;
    }

    public void setDateOfWorkOnTask(LocalDate dateOfWorkOnTask) {
        this.dateOfWorkOnTask = dateOfWorkOnTask;
    }
    
    @ManyToOne
    @JoinColumn(name = "task_id")
    public TaskEntity getTaskEntity() {
        return taskEntity;
    }

    public void setTaskEntity(TaskEntity taskEntity) {
        this.taskEntity = taskEntity;
    }
    
    
    
    
    
    
    
    
    
    
    
}
